export default({$axios, redirect})=>{
  // 请求拦截
  $axios.onRequest(req=>{
    console.log(req);
  })
  // 响应拦截
  $axios.onResponse(res=>{
    console.log(res);
  })
  // 错误拦截
  $axios.onError(err=>{
    const code = parseInt(error.response && error.response.status)
    if (code === 400) {
      redirect('/400')
    }
  })
}