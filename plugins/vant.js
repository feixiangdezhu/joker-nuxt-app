import Vue from 'vue'

import {
  Field,
  Cell,CellGroup,
  Button,
  Icon,
  Tabbar,TabbarItem,
  Search,
  Swipe, SwipeItem,
  Grid, GridItem,
  NoticeBar,
  NavBar,
  Tab, Tabs,
  List,
  PullRefresh,
  ImagePreview,
  CountDown,
  GoodsAction, GoodsActionIcon, GoodsActionButton,
  Popup,
  Lazyload,
  Empty,
  Checkbox, CheckboxGroup,
  Stepper,
  SubmitBar,
  Tag
} from 'vant'

const arrComponents=[
  Field,
  Cell,CellGroup,
  Button,
  Icon,
  Tabbar,TabbarItem,
  Search,
  Swipe, SwipeItem,
  Grid, GridItem,
  NoticeBar,
  NavBar,
  Tab, Tabs,
  List,
  PullRefresh,
  ImagePreview,
  CountDown,
  GoodsAction, GoodsActionIcon, GoodsActionButton,
  Popup,
  Lazyload,
  Empty,
  Checkbox, CheckboxGroup,
  Stepper,
  SubmitBar,
  Tag
]

import 'vant/lib/index.css';

for (let i of arrComponents){
  Vue.use(i)
}
