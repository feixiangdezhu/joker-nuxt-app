export default ({ app }) => {
  app.router.beforeEach((to, from, next) => {
    
    const isToken=app.$cookies.get("token")
    if (!isToken  && (to.path == '/cart' || to.path == '/ucenter') ) {
      next('/login');
    } else {
      next();
    }
    
  });
};