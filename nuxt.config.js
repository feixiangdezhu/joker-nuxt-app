const env = require('./env');
module.exports = {
	mode: 'universal',
	env: {
		base_url: env[process.env.NODE_ENV].base_url,
		host_name: env[process.env.NODE_ENV].host_name
	},
	/*
     ** Headers of the page
     */
	head: {
		title: process.env.npm_package_name || '',
		meta: [
			{
				charset: 'utf-8'
			},
			{
				name: 'viewport',
				content: 'width=device-width, initial-scale=1'
			},
			{
				hid: 'description',
				name: 'description',
				content: process.env.npm_package_description || ''
			}
		],
		link: [
			{
				rel: 'icon',
				type: 'image/x-icon',
				href: '/favicon.ico'
			}
		],
		script: [
			{
				src: '/js/flexible.js',
				type: 'text/javascript',
				charset: 'utf-8'
			}
		]
	},
	/*
     ** Customize the progress-bar color
     */
	loading: {
		color: '#fff'
	},
	/*
     ** Global CSS
     */
	css: [ 'element-ui/lib/theme-chalk/index.css', 'normalize.css','~/assets/style/whole.css','~/assets/style/iconfont/iconfont.css', '~/assets/style/index.scss'],
	/*
     ** Plugins to load before mounting the App
     */
	plugins: [
		'@/plugins/element-ui',
		'@/plugins/vant',
    '@/plugins/router',
		{
			src: '@/plugins/vant',
			ssr: true
		},
		{
			src: '~/plugins/axios',
			ssr: true
		}
	],
	components: true,
	/*
     ** Nuxt.js dev-modules
     */
	buildModules: [
		// Doc: https://github.com/nuxt-community/eslint-module
		// '@nuxtjs/eslint-module'
		'@nuxtjs/style-resources'
	],
	styleResources: {
		// your settings here
		scss: [ './assets/style/variable.scss' ],
		hoistUseStatements: true
	},
	/*
     ** Nuxt.js modules
     */
	modules: [
		// Doc: https://axios.nuxtjs.org/usage
		'@nuxtjs/axios',
		'@nuxtjs/proxy',
    'cookie-universal-nuxt',
    ['cookie-universal-nuxt', { alias: 'cookiz' }],
	],
	/*
     ** Axios module configuration
     ** See https://axios.nuxtjs.org/options
     */
	axios: {
		proxy: true
	},
	proxy: {
		// '/api':{
		//   target:'http://exple.com',
		//   changeOrigin:true
		// }
	},
	/*
     ** Build configuration
     */
	build: {
		transpile: [ /^element-ui/ ],
		/*
         ** You can extend webpack config here
         */
		extend(config, ctx) {},
		vendor: [ 'axios' ],
		postcss: [ require('postcss-pxtorem')({ rootValue: 37.5, propList: [ '*' ] }) ],
		babel: {
			plugins: [
				[
					'import',
					{
						libraryName: 'vant',
						style: false
					},
					'vant'
				]
			]
		}
	},
	router: {
		middleware: ['redirect'],
	},
	telemetry: false
};
