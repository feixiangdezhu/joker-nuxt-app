export default function ({ app, route, redirect }) {
  
  const isToken=app.$cookies.get("token")
    if (!isToken  && (route.path == '/cart' || route.path == '/ucenter') ) {
      redirect('/login');
    }
  
}