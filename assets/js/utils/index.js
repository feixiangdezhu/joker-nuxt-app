export function throttler(fn, time) {
  let timeOut = null;
  // 创建闭包
  return function () {
    clearTimeout(timeOut);
    timeOut = setTimeout(() => {
      fn.apply(this, arguments);
    }, time);
  };
}