function order500(orderType,isPay,count) {
  if (orderType===1 && isPay==true){
    console.log('亲爱的用户，您中奖了20元');
  } else {
    return 'next'
  }  
}
function order200(orderType,isPay,count) {
  if (orderType===2 && isPay==true){
    console.log('亲爱的用户，您中奖了15元');
  } else {
    return 'next'
  }  
}
function order100(orderType,isPay,count) {
  if (orderType===4 && isPay==true){
    console.log('亲爱的用户，您中奖了10元');
  } else {
    return 'next'
  }  
}
function orderNormal(orderType,isPay,count) {
  if (count > 0){
    console.log('亲爱的用户，您中奖了2元');
  } else {
    console.log('亲爱的用户，再接再厉');
  }  
}



class Chain {
  constructor(fn){
    this.fn=fn
    this.next=null
  }
  setNext(nextChain){
    this.next=nextChain
  }
  passRequest(){
    let res=this.fn.apply(this,arguments)
    if (res=='next'){
      return this.next && this.next.passRequest(this.next,arguments)
    }
    return res
  }
}

let chainOrder500=new Chain(order500)
let chainOrder200=new Chain(order200)
let chainOrder100=new Chain(order100)
let chainOrderNormal=new Chain(orderNormal)

chainOrder500.setNext(chainOrder200)
chainOrder200.setNext(chainOrder100)
chainOrder100.setNext(chainOrderNormal)

chainOrder500.passRequest(1,true,0)
chainOrder500.passRequest(1,true,100)
chainOrder500.passRequest(1,false,10)
chainOrder500.passRequest(1,true,0)
chainOrder500.passRequest(1,true,0)
chainOrder500.passRequest(1,true,0)